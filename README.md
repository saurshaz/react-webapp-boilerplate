

### Steps to run the sample:
Use the scripts in `package.json` to run the app, to run the tests

1. `yarn start` to start the app in dev mode
2. Launch the app from http://localhost:8080

### Steps to test the sample:
Use the scripts in `package.json` to run the app, to run the tests

1. `yarn test` to run the unit tests

### Steps to run e2e tests:
Use the scripts in `package.json` to run the app, to run the tests

1. `yarn e2e-test` to run the e2e tests


## TODOs
- [X] use `webpack` for build
- [X] have styles usable at component level
- [ ] add `tests`
- [ ] use `hooks` in the example
- [ ] remove files that are clutter
- [ ] run in a container (`kubernates`)
- [ ] add a mock mode, 
- [ ] add `redux` usage and `thunk`
- [ ] add an api call